import aiohttp
import tempfile
import pathlib
import xml.dom.minidom
import os
import io
import interactions

import config
import scriptdata

bot = interactions.Client(token=config.get_token())

delete_this = interactions.Button(
	style=interactions.ButtonStyle.DANGER,
	label="Delete This!",
	custom_id="delete_this"
)

@bot.component("delete_this")
async def delete_this_response(ctx):
    await ctx.message.delete("Delete Button!")

@bot.command(type=interactions.ApplicationCommandType.MESSAGE, name="Convert Script Data")
async def convert_script_data(ctx):
	if ctx.target.attachments == None or len(ctx.target.attachments) == 0:
		await ctx.send(f"No Files!", ephemeral=True)
		return

	session = aiohttp.ClientSession()

	files = []
	for attachment in ctx.target.attachments:
		file_name = pathlib.Path(os.path.basename(attachment.url))
		file_data = await (await session.get(attachment.url)).read()

		script_data_test = scriptdata.DieselScriptData()
		script_data_test.read(file_data)

		pretty_xml = xml.dom.minidom.parseString(script_data_test.custom_xml).toprettyxml()

		output = io.StringIO(pretty_xml[23:])
		file = interactions.File(filename=str(file_name.with_suffix(".xml")), fp=output)

		files.append(file)

	await session.close()

	await ctx.send("Converting...")
	await ctx.message.edit("", components=delete_this, files=[file])

	for file in files:
		file._fp.close()

	output.close()

bot.start()