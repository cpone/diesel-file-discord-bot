import json

config_data = None

with open("config.json", "r") as file:
	config_data = json.load(file)

def get_token():
	return config_data["token"]